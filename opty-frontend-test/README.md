#Front-end test

## Status

### Validation

Form validation urrently relies on the validation done by the browser thanks to **HTML5** `<input>` types, e.g. `number`, `color`. Issues here:

  * `<input type='color'>` not supported by **IE**, so the user would be expected to add in the hexadecimal colour code themselves, including the `#` character, as opposed to **FF** and **Chrome** which support this and automaticaly prepend the colour code with the character. Possible fixes for this include a custom validation of input to check for the `#`.
  * `<input pattern=''>` not supported by **IE9**. Possible fix for this would be to have a custom validator method to check the pattern.
  * `<input type='number'>` not supported by **IE9**. Same as before.

### Other requirements

  * **Make** field does not have any images attached to the data source, but the layout to support them is done.

### Other improvements

  * Modal header - the layout for the title, subtitle, and category could be improved, but I did not have any ideas for them on the spot.
  * Form elements alignment - there are a few differences in width on some rows.
  * Overall look and feel - could be improved, as it might be a bit bland at the moment :<

## Specs

You're asked to create **a modal window that will open "on page"** according to the specs below

### Modal's content

#### Header

  * Modal title
  * Subtitle
  * Category

#### Form section 1

  * Title of announcement
  * Make / brand
  * Manufacturing year
  * Mileage
  * Fuel type
  * Color

#### Form section 2

  * Vehicle damaged (yes/no)
  * Price
  * Currency
  * Description
  * Save / Cancel

The modal should also have areas where to display warning/error and help messages.

### Implementation requirements
You have to use HTML, CSS and JavaScript to implement the modal window, but no preprocessors, libraries nor frameworks (e.g. Sass, LESS, etc. for CSS or jQuery, AngularJS, BackboneJS, etc. for JavaScript). You’re encouraged to use HTML5 and its JavaScript APIs, also the modal window should have a cross-browser implementation among modern browsers (and IE9+).

Beside these, you have to make sure you're following the requirements below:

  * Validation for all fields (according to their nature) by marking the appearance for valid/invalid fields
  * Mileage Field – validated if it is number when the field is completed
  * Make/Brand Field – when selecting a make/brand, its logo should appear somewhere on the page 
  * Color Field – when filled in, a square representing the graphic color should appear (do not use image) 
  * Vehicle damaged – when ”Yes” selected - another field should appear where user could add details about the crash, to be validated. When “No” selected - the details field will not be visible.

We'll consider **page aesthetic** and user experience as a plus.