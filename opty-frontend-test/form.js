function FormPopulate(config) {
    config = config || {};

    var form = config.form;
    var dataSources = config.data || {};
    var dependencies = config.dependencies || [];
    var computedFields = config.computedFields || [];

    var selects = form.querySelectorAll('select');
    for (var i = 0; i < selects.length; ++i) {
        var select = selects[i];
        var source = dataSources[select.getAttribute('data-source')];

        select.add(new Option("Please select a value"));
        source.forEach(function(el) {
            select.add(new Option(el.name, el.id || el.name));
        });
    }

    dependencies.forEach(function(dep) {
        var dependsOn = form.querySelector(dep.dependsOn);
        var dependent = form.querySelector(dep.dependent);

        dependsOn.addEventListener('change', function() {
            if (dependsOn.checked) {
                dependent.parentNode.classList.remove('hide');
                dependent.setAttribute('required', true);
            }
            else {
                dependent.parentNode.classList.add('hide');
                dependent.removeAttribute('required');
            }
        });
    });

    computedFields.forEach(function(pair) {
        var field = form.querySelector(pair.field);
        var type = pair.computedType;
        var dataKey = pair.dataKey || '';
        var source = dataSources[field.getAttribute('data-source')] || [];

        field.addEventListener('change', updateComputed(type, field, source, dataKey));
    });

    function updateComputed(type, field, dataSource, dataKey) {
        return function() {
            var value = field.value;
            var computedEl = field.parentNode.querySelector('.computed');

            if (type === 'image') {
                var dataObject = dataSource.filter(function(el) { return el.id === value; }).pop();
                var imageSource = dataObject[dataKey] || '';

                computedEl.style.backgroundImage = "url('"+ imageSource +"') no-repeat";
            }
            else if (type === 'color') {
                computedEl.style.backgroundColor = value;
            }
        };
    }
}
