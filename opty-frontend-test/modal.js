function ModalInit(modal, config) {
    config = config || {};
    var defaultConfig = {
        width: '500px'
    };

    var form = modal.querySelector('form');

    // setup size
    modal.style.width = config.width || defaultConfig.width;

    // setup overlay
    var overlay = createOverlay(modal);

    // display
    modal.classList.remove('hide');

    var cancel = modal.querySelector('input[type="button"][value="Cancel"]');
    cancel.addEventListener("click", function() {
        form.reset();
        modal.classList.add('hide');
        destroyOverlay(overlay);
    });

    function createOverlay(modal) {
        var overlay = document.createElement("section");
        overlay.classList.add('overlay');
        modal.parentNode.insertBefore(overlay, modal);

        return overlay;
    }

    function destroyOverlay(overlay) {
        overlay.parentNode.removeChild(overlay);
    }
}
